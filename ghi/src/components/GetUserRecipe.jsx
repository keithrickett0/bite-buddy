import { useGetUserRecipeQuery } from "../app/apiSlice";


const GetUserRecipe = () => {
    const { data, isLoading } = useGetUserRecipeQuery

    if (isLoading) return <div>
        Loading...
    </div>

    return <div>list user recipe working</div>
}

export default GetUserRecipe
