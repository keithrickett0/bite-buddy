import { useUpdateRecipeMutation } from "../app/apiSlice";


const UpdateRecipe = () => {
    const { data, isLoading } = useUpdateRecipeMutation

    if (isLoading) return <div>
        Loading...
    </div>

    return <div>update recipe endpoint working</div>
}

export default UpdateRecipe
