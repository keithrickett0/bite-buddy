import { useDeleteRecipeMutation } from "../app/apiSlice";


const DeleteRecipe = () => {
    const { data, isLoading } = useDeleteRecipeMutation

    if (isLoading) return <div>
        Loading...
    </div>

    return <div>delete recipe endpoint working</div>
}

export default DeleteRecipe
