import { useListUserRecipesQuery } from "../app/apiSlice";


const ListUserRecipes = () => {
    const { data, isLoading } = useListUserRecipesQuery

    if (isLoading) return <div>
        Loading...
    </div>

    return <div>List user endpoint working</div>
}

export default ListUserRecipes
