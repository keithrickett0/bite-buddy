import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

export const recipeApi = createApi({
    reducerPath: 'recipeApi',
    baseQuery: fetchBaseQuery({
        baseUrl: import.meta.env.VITE_API_HOST,
        credentials: 'include',
    }),
    tagTypes: ['Recipes', 'User'],
    endpoints: (builder) => ({

        createRecipe: builder.mutation({
            query: (newRecipe) => ({
                url: '/api/recipes',
                method: 'POST',
                body: newRecipe,
            }),
            invalidatesTags: ['Recipes'],
        }),

        listUserRecipes: builder.query({
            query: () => ({
                url: '/api/recipes'
            }),
            providesTags: 'Recipes'
        }),

        updateRecipe: builder.mutation({
            query: ({ recipe_id, body }) => ({
                url: `/api/recipes/${recipe_id}`,
                method: 'PUT',
                body: body
            }),
            invalidatesTags: (result, error, recipe_id) => [
                {type: 'Recipes', id: recipe_id}
            ],
        }),

        deleteRecipe: builder.mutation({
            query: (recipe_id) => ({
                url: `/api/recipes/${recipe_id}`,
                method: 'DELETE'
            }),
            invalidatesTags: (result, error, recipe_id) => [
                {type: 'Recipes', id: recipe_id}
            ],
        }),

        getUserRecipe: builder.query({
            query: (recipe_id) => ({
                url: `/api/recipes/${recipe_id}`
            }),
            providesTags: (result, error, recipe_id) => [
                {type: 'Recipes', id: recipe_id}
            ],
        }),

        getUser: builder.query({
            query: () => ({
                url: '/api/auth/authenticate',
            }),
            providesTags: ['User'],
        }),

        signUp: builder.mutation({
            query: (userData) => ({
                url: '/api/auth/signup',
                method: 'POST',
                body: userData,
            }),
            invalidatesTags: ['User'],
        }),

        signIn: builder.mutation({
            query: (credentials) => ({
                url: '/api/auth/signin',
                method: 'POST',
                body: credentials,
            }),
            invalidatesTags: ['User'],
        }),

        signOut: builder.mutation({
            query: () => ({
                url: '/api/auth/signout',
                method: 'DELETE',
            }),
            invalidatesTags: ['User'],
        }),
    })
})

export const {
    useCreateRecipeMutation,
    useListUserRecipesQuery,
    useUpdateRecipeMutation,
    useDeleteRecipeMutation,
    useGetUserRecipeQuery,
    useGetUserQuery,
    useSignInMutation,
    useSignOutMutation,
    useSignUpMutation,
} = recipeApi
