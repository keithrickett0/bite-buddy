steps = [
    # Create recipe_cuisine table
    [
        """
        CREATE TABLE recipe_category (
            id SERIAL PRIMARY KEY NOT NULL,
            name TEXT NOT NULL
        );
        """,
        ""
    ],

    [
        """
        INSERT INTO recipe_category (name)
        VALUES
            ('Appetizers'),
            ('Bread'),
            ('Desserts'),
            ('Drinks'),
            ('Main Dish'),
            ('Salad'),
            ('Side Dish'),
            ('Soups, Stews and Chili'),
            ('Marinades and Sauces'),
            ('Other');
        """,
        ""
    ],
    # Create recipe table
    [
        """
        CREATE TABLE recipe (
            id SERIAL PRIMARY KEY NOT NULL,
            user_id INT NOT NULL REFERENCES users(id),
            recipe_category_id INT NOT NULL REFERENCES recipe_category(id),
            cuisine TEXT,
            main_ingredient TEXT,
            description TEXT,
            prep_time_minutes INT,
            cook_time_minutes INT,
            ingredients TEXT[],
            instructions TEXT
        );
        """,
        ""
    ],
]
