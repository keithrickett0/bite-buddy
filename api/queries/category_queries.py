from pydantic import BaseModel
import os
from psycopg_pool import ConnectionPool
from typing import List, Union
from psycopg.rows import class_row
from models.category import CategoryOut


DATABASE_URL = os.environ.get("DATABASE_URL")
if not DATABASE_URL:
    raise ValueError("DATABASE_URL environment variable is not set")


pool = ConnectionPool(DATABASE_URL)


class Error(BaseModel):
    message: str


class CategoryRepository:
    def list_all(self) -> Union[List[CategoryOut], Error]:
        """
        List all categories.

        Returns:
            Union[List[CategoryOut], Error]: A list of all categories,
            or an error messages.
        """
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=class_row(CategoryOut)) as db:
                    db.execute(
                        """
                        SELECT
                            id,
                            name
                        FROM recipe_category
                        """
                    )
                    categories = db.fetchall()
                return [
                    CategoryOut(
                        id=category.id,
                        name=category.name
                    )
                    for category in categories
                ]
        except Exception as e:
            print(f"An error occurred: {e}")
            return Error(message="Could not retrieve categories.")
