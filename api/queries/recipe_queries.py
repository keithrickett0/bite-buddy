from pydantic import BaseModel
import os
from psycopg_pool import ConnectionPool
from typing import List, Union
from psycopg.rows import class_row
from models.recipe import RecipeIn, RecipeOut


DATABASE_URL = os.environ.get("DATABASE_URL")
if not DATABASE_URL:
    raise ValueError("DATABASE_URL environment variable is not set")


pool = ConnectionPool(DATABASE_URL)


class Error(BaseModel):
    message: str


class RecipeRepository:
    def create(
            self,
            recipe: RecipeIn,
            user_id: int
            ) -> Union[RecipeOut, Error]:
        """
        Create a new recipe in the database.

        Parameters:
            recipe (RecipeIn): The recipe data to be inserted.
            user_id (int): The ID of the user creating the recipe.

        Returns:
            Union[RecipeOut, Error]: The created recipe or an error message.
        """
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=class_row(RecipeOut)) as db:
                    db.execute(
                        """
                        INSERT INTO recipe
                            ( recipe_category_id,
                            cuisine,
                            main_ingredient,
                            description,
                            prep_time_minutes,
                            cook_time_minutes,
                            ingredients,
                            instructions,
                            user_id )
                        VALUES
                            (%s, %s, %s, %s, %s, %s, %s, %s, %s)
                        RETURNING
                            id AS recipe_id,
                            recipe_category_id AS category,
                            cuisine,
                            main_ingredient,
                            description,
                            prep_time_minutes,
                            cook_time_minutes,
                            ingredients,
                            instructions;
                        """,
                        [
                            recipe.category,
                            recipe.cuisine,
                            recipe.main_ingredient,
                            recipe.description,
                            recipe.prep_time_minutes,
                            recipe.cook_time_minutes,
                            recipe.ingredients,
                            recipe.instructions,
                            user_id
                        ]
                    )
                    result = db.fetchone()
                    return result
        except Exception as e:
            return Error(message=str(e))

    def update(
            self,
            recipe_id: int,
            recipe: RecipeIn,
            user_id: int) -> Union[RecipeOut, Error]:
        """
        Update an existing recipe in the database.

        Parameters:
            recipe_id (int): The ID of the recipe to be updated.
            recipe (RecipeIn): The recipe data to be inserted.
            user_id (int): The ID of the user updating the recipe.

        Returns:
            Union[RecipeOut, Error]: The updated recipe or an error message.
        """
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=class_row(RecipeOut)) as db:
                    db.execute(
                        """
                        UPDATE recipe
                        SET recipe_category_id = %s,
                            cuisine = %s,
                            main_ingredient = %s,
                            description = %s,
                            prep_time_minutes = %s,
                            cook_time_minutes = %s,
                            ingredients = %s,
                            instructions = %s
                        WHERE id = %s AND user_id = %s
                        RETURNING
                            id AS recipe_id,
                            recipe_category_id AS category,
                            cuisine,
                            main_ingredient,
                            description,
                            prep_time_minutes,
                            cook_time_minutes,
                            ingredients,
                            instructions;
                        """,
                        [
                            recipe.category,
                            recipe.cuisine,
                            recipe.main_ingredient,
                            recipe.description,
                            recipe.prep_time_minutes,
                            recipe.cook_time_minutes,
                            recipe.ingredients,
                            recipe.instructions,
                            recipe_id,
                            user_id
                        ]
                    )
                    result = db.fetchone()
                    return result
        except Exception as e:
            return Error(message=str(e))

    def delete(self, recipe_id: int, user_id: int) -> Union[bool, Error]:
        """
        Delete an existing recipe in the database.

        Parameters:
            recipe_id (int): The ID of the recipe to be deleted.
            user_id (int): The ID of the user attempting to delete the recipe.

        Returns:
            Union[bool, Error]: True if the deletion was successful,
            or an error message.
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM recipe
                        WHERE
                            id = %s AND user_id = %s
                        """,
                        [
                            recipe_id,
                            user_id
                        ]
                    )
                    if db.rowcount == 0:
                        return Error(
                            message="Recipe not found or user not authorized."
                        )
                    return True
        except Exception as e:
            return Error(message=str(e))

    def list(self, user_id: int) -> Union[List[RecipeOut], Error]:
        """
        List a specific users recipes.

        Parameters:
            user_id (int): The ID of the user that is veiwing their recipes.

        Returns:
            Union[List[RecipeOut], Error]: A list of users recipes,
            or an error message.
        """
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=class_row(RecipeOut)) as db:
                    db.execute(
                        """
                        SELECT
                            id AS recipe_id,
                            recipe_category_id AS category,
                            cuisine,
                            main_ingredient,
                            description,
                            prep_time_minutes,
                            cook_time_minutes,
                            ingredients,
                            instructions
                        FROM recipe
                        WHERE user_id = %s
                        """,
                        [
                            user_id
                        ]
                    )
                    result = db.fetchall()
                    return result
        except Exception as e:
            return Error(message=str(e))

    def get(self, recipe_id: int, user_id: int) -> Union[RecipeOut, Error]:
        """
        Get a specific recipe for a user.

        Parameters:
            recipe_id (int): The ID of the recipe the user is trying to view.
            user_id (int): The ID of the user that is veiwing their recipes.

        Returns:
            Union[RecipeOut, Error]: A users recipe, or an error message.
        """
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=class_row(RecipeOut)) as db:
                    db.execute(
                        """
                        SELECT
                            id AS recipe_id,
                            recipe_category_id AS category,
                            cuisine,
                            main_ingredient,
                            description,
                            prep_time_minutes,
                            cook_time_minutes,
                            ingredients,
                            instructions
                        FROM recipe
                        WHERE id = %s AND user_id = %s
                        """,
                        [
                            recipe_id,
                            user_id
                        ]
                    )
                    result = db.fetchone()
                    if result is None:
                        return Error(
                            message="Recipe not found or user not authorized"
                        )
                    return result
        except Exception as e:
            return Error(message=str(e))

    def list_all(self) -> Union[List[RecipeOut], Error]:
        """
        List all recipes from all users.

        Returns:
            Union[List[RecipeOut], Error]: A list of all recipes,
            or an error message.
        """
        try:
            with pool.connection() as conn:
                print(conn, "********")
                with conn.cursor(row_factory=class_row(RecipeOut)) as db:
                    print(db, "********")
                    db.execute(
                        """
                        SELECT
                            id AS recipe_id,
                            recipe_category_id AS category,
                            cuisine,
                            main_ingredient,
                            description,
                            prep_time_minutes,
                            cook_time_minutes,
                            ingredients,
                            instructions
                        FROM recipe
                        """
                    )
                    recipes = db.fetchall()
                return [
                    RecipeOut(
                        recipe_id=recipe.recipe_id,
                        category=recipe.category,
                        cuisine=recipe.cuisine,
                        main_ingredient=recipe.main_ingredient,
                        description=recipe.description,
                        prep_time_minutes=recipe.prep_time_minutes,
                        cook_time_minutes=recipe.cook_time_minutes,
                        ingredients=recipe.ingredients,
                        instructions=recipe.instructions
                    )
                    for recipe in recipes
                ]
        except Exception as e:
            print(f"An error occurred: {e}")
            return Error(message="Could not retrieve recipes.")
