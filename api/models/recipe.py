from typing import Optional
from pydantic import BaseModel


class RecipeIn(BaseModel):
    category: int
    cuisine: str
    main_ingredient: str
    description: Optional[str]
    prep_time_minutes: int
    cook_time_minutes: int
    ingredients: list[str]
    instructions: str


class RecipeOut(BaseModel):
    recipe_id: int
    category: int
    cuisine: str
    main_ingredient: str
    description: Optional[str]
    prep_time_minutes: int
    cook_time_minutes: int
    ingredients: list[str]
    instructions: str
