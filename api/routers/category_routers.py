from fastapi import APIRouter, Depends, HTTPException
from typing import List
from models.category import CategoryOut
from queries.category_queries import CategoryRepository, Error


router = APIRouter(
    tags=["Recipes"],
    prefix="/api/categories"
)


@router.get("", response_model=List[CategoryOut])
def list_categories(
    repo: CategoryRepository = Depends()
):
    """
    Retrieve and return a list of all categories in the database.
    """
    result = repo.list_all()
    if isinstance(result, Error):
        raise HTTPException(
            status_code=400,
            detail=result.messgae
            )
    return result
