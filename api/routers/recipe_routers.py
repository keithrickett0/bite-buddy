from fastapi import APIRouter, Depends, HTTPException
from typing import List
from models.recipe import RecipeIn, RecipeOut
from models.users import UserResponse
from utils.authentication import try_get_jwt_user_data
from queries.recipe_queries import RecipeRepository, Error


router = APIRouter(
    tags=["Recipes"],
    prefix="/api/recipes"
)


@router.post("", response_model=RecipeOut)
def create_recipe(
    recipe: RecipeIn,
    user: UserResponse = Depends(try_get_jwt_user_data),
    repo: RecipeRepository = Depends(),
):
    """
    Ensure the user is authenicated.
    Raise a 401 error if the user if not authenticated.
    Create a new recipe associated with the authenticated user.
    """
    if not user:
        raise HTTPException(
            status_code=401,
            detail="Must be logged in to create a recipe."
        )
    return repo.create(recipe=recipe, user_id=user.id)


@router.get("", response_model=List[RecipeOut])
def list_user_recipes(
    user: UserResponse = Depends(try_get_jwt_user_data),
    repo: RecipeRepository = Depends()
):
    """
    Ensure the user is authenicated.
    Raise a 401 error if the user if not authenticated.
    Return a list of recipes associated with the authenticated user.
    """
    if not user:
        raise HTTPException(
            status_code=401,
            detail="Must be logged in to view your recipes."
        )
    return repo.list(user_id=user.id)


@router.get("/all", response_model=List[RecipeOut])
def list_all_recipes(
    repo: RecipeRepository = Depends()
):
    """
    Retrieve and return a list of all recipes in the database.
    """
    result = repo.list_all()
    return result


@router.put("/{recipe_id}", response_model=RecipeOut)
def update_recipe(
    recipe_id: int,
    recipe: RecipeIn,
    user: UserResponse = Depends(try_get_jwt_user_data),
    repo: RecipeRepository = Depends()
):
    """
    Ensure the user is authenicated.
    Raise a 401 error if the user if not authenticated.
    Attempt to update the specified recipe.
    Raise a 404 error with the error message if the update fails.
    Return the updated recipe if successful.
    """
    if not user:
        raise HTTPException(
            status_code=401,
            detail="Must be logged in to update this recipe."
        )
    result = repo.update(
        recipe_id=recipe_id,
        recipe=recipe,
        user_id=user.id
    )
    if isinstance(result, Error):
        raise HTTPException(
            status_code=404,
            detail=result.message
        )
    return result


@router.delete("/{recipe_id}", response_model=bool)
def delete_recipe(
    recipe_id: int,
    user: UserResponse = Depends(try_get_jwt_user_data),
    repo: RecipeRepository = Depends()
):
    """
    Ensure the user is authenicated.
    Raise a 401 error if the user if not authenticated.
    Attempt to delete the specified recipe.
    Raise a 404 error with the error message if the deletion fails.
    Return True if the deletion is successful.
    """
    if not user:
        raise HTTPException(
            status_code=401,
            detail="Must be logged in to delete a recipe."
        )
    result = repo.delete(recipe_id=recipe_id, user_id=user.id)
    if isinstance(result, Error):
        raise HTTPException(
            status_code=404,
            detail=result.message
        )
    return result


@router.get("/{recipe_id}", response_model=RecipeOut)
def get_user_recipe(
    recipe_id: int,
    user: UserResponse = Depends(try_get_jwt_user_data),
    repo: RecipeRepository = Depends()
):
    """
    Ensure the user is authenicated.
    Raise a 401 error if the user if not authenticated.
    Retrieve the specified recipe associated with the authenicated user.
    Raise a 404 error with the error message if the retrieval fails.
    Return the recipe if successful.
    """
    if not user:
        raise HTTPException(
            status_code=401,
            detail="Must be logged in to veiw this recipe."
        )
    result = repo.get(recipe_id=recipe_id, user_id=user.id)
    if isinstance(result, Error):
        raise HTTPException(
            status_code=404,
            detail=result.message
        )
    return result
